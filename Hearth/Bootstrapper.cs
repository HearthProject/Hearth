﻿using System.Collections.Generic;
using System.Security.Claims;
using Hearth.Configuration;
using Hearth.Services.Auth;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.Configuration;
using Nancy.TinyIoc;
using Nancy.Authentication.Stateless;

namespace Hearth
{
    public class Bootstrapper : DefaultNancyBootstrapper
    {
        public override void Configure(INancyEnvironment environment)
        {
            base.Configure(environment);
#if DEBUG
            environment.Views(true, true);
#endif
        }

        protected override void ApplicationStartup(TinyIoCContainer containr, IPipelines pipelines)
        {
            StatelessAuthentication.Enable(pipelines, new StatelessAuthenticationConfiguration(ctx =>
            {
                var authHeader = ctx.Request.Headers.Authorization;
                if (string.IsNullOrWhiteSpace(authHeader)) return null;
                var userManager = new UserManagerService();
                var user = userManager.FindUserByApiKeyAsync(authHeader).Result;
                if (user == null) return null;
                var userAuthClaims = new List<Claim>
                {
                    new Claim("user_id", user.UUID),
                    new Claim("access_type", user.Role.ToString())
                };
                var userAuthIdentity = new ClaimsIdentity(userAuthClaims);
                return new ClaimsPrincipal(userAuthIdentity);
            }));
            
            
            pipelines.AfterRequest.AddItemToEndOfPipeline(ctx =>
            {
                foreach (var origin in HearthContext.HearthConfig.CorsOrigins)
                {
                    ctx.Response.WithHeader("Access-Control-Allow-Origin", origin);
                }
                ctx.Response
                    .WithHeader("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE")
                    .WithHeader("Access-Control-Allow-Headers", "Accept, Origin, Content-Type, Authorization");
            });
        }
    }
}