﻿using System.Collections.Generic;
using Hearth.Models;
using Newtonsoft.Json;

namespace Hearth.Configuration
{
    public class HearthConfiguration
    {
        [JsonProperty("initialAdmin")]
        public string InitialAdmin { get; set; } = string.Empty;

        [JsonProperty("webhookPath")]
        public string WebhookPath { get; set; } = string.Empty;

        [JsonProperty("databaseFile")]
        public string DatabaseFile { get; set; } = "hearth.lidb";

        [JsonProperty("corsOrigins")]
        public string[] CorsOrigins { get; set; } = new string[0];

        [JsonProperty("maxFileSize")]
        public int[] MaxFileSize { get; set; } = {250, 250, 250, 250, 250};

        [JsonProperty("maxTotalSize")]
        public int[] MaxTotalSize { get; set; } = {500, 5120, 25600, 25600, 25600};

        [JsonProperty("inviteRequired")]
        public bool InviteRequired { get; set; }

        [JsonProperty("instanceName")]
        public string InstanceName { get; set; } = "Hearth";

        [JsonProperty("roleNames")]
        public string[] RoleNames { get; set; } = {"Guest", "User", "Trusted", "Moderator", "Admin"};
        
        [JsonProperty("fireGuardCommand")]
        public string FireGuardCommand { get; set; }

        [JsonProperty("cacheDir")]
        public string CacheDir { get; set; } = "cache";

        [JsonProperty("dataDir")]
        public string DataDir { get; set; } = "data";

        [JsonProperty("storageUrl")]
        public string StorageUrl { get; set; } = string.Empty;

        [JsonProperty("baseUrl")]
        public string BaseUrl { get; set; } = string.Empty;

        [JsonProperty("sources")]
        public List<OneMetaSource> Sources { get; set; } = new List<OneMetaSource>();
    }
}