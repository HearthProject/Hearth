﻿using Hearth.Infrastructure.Concurrency;
using Hearth.Services.Analytics;
using Hearth.Services.Auth;
using Hearth.Services.Storage;
using LiteDB;

namespace Hearth.Configuration
{
    public class HearthContext
    {
        public static LiteDatabase Database { get; set; }
        
        public static HearthConfiguration HearthConfig { get; set; }
        
        public static UserManagerService UserManager { get; set; }
        
        public static AnalyticsDatabase AnalyticsDB { get; set; }
        
        public static FileStorageService FileStorage { get; set; }
        
        public static UserServiceTable UserServiceTable { get; set; }
        
        public static FeaturedModpackService FeaturedModpacks { get; set; }
        
        public static LogStorageService LogStorage { get; set; }
    }
}