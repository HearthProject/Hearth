﻿using System.Collections.Generic;
using System.Linq;
using Hearth.Models;
using Newtonsoft.Json;

namespace Hearth.Configuration
{
    public class PublicHearthConfig
    {
        [JsonProperty("maxFileSize")]
        public int[] MaxFileSize { get; set; }
        
        [JsonProperty("maxTotalSize")]
        public int[] MaxTotalSize { get; set; }
        
        [JsonProperty("inviteRequired")]
        public bool InviteRequired { get; set; }
        
        [JsonProperty("instanceName")]
        public string InstanceName { get; set; }
        
        [JsonProperty("roleNames")]
        public string[] RoleNames { get; set; }
        
        [JsonProperty("users")]
        public int Users { get; set; }

        [JsonProperty("sources")]
        public List<OneMetaSource> Sources { get; set; }

        public PublicHearthConfig(HearthConfiguration config)
        {
            MaxFileSize = config.MaxFileSize;
            MaxTotalSize = config.MaxTotalSize;
            InviteRequired = config.InviteRequired;
            InstanceName = config.InstanceName;
            RoleNames = config.RoleNames;
            Users = HearthContext.UserManager.GetUserCount();
            Sources = config.Sources;
        }
    }
}