﻿using System.Collections.Concurrent;

namespace Hearth.Infrastructure.Concurrency
{
    public class UserServiceTable
    {
        private ConcurrentDictionary<string, UserServices> serviceTable = new ConcurrentDictionary<string, UserServices>();

        public UserServices GetOrCreate(string username)
        {
            return serviceTable.GetOrAdd(username, key => new UserServices(username));
        }

        public UserServices this[string username] => GetOrCreate(username);
    }
}