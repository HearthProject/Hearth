﻿using Hearth.Models.Request;
using Newtonsoft.Json;

namespace Hearth.Models.Analytics
{
    public enum EventType
    {
        Installed,
        Launched,
        PackInstalled,
        InstanceCreated,
        ModInstalled
    }
    
    public class AnalyticsEvent : DatabaseObject
    {
        [JsonProperty("uuid")]
        public string UUID { get; set; }
        
        [JsonProperty("version")]
        public string Version { get; set; }
        
        [JsonProperty("builder")]
        public string Builder { get; set; }
        
        [JsonProperty("osName")]
        public string OSName { get; set; }
        
        [JsonProperty("arch")]
        public string Arch { get; set; }
        
        [JsonProperty("memory")]
        public int Memory { get; set; }
        
        [JsonProperty("event")]
        public EventType Event { get; set; }
        
        [JsonProperty("extra")]
        public string Extra { get; set; }
        
        [JsonProperty("addr")]
        public string Addr { get; set; }

        public AnalyticsEvent(AnalyticsEventRequest req)
        {
            UUID = req.UUID;
            Version = req.Version;
            Builder = req.Builder;
            OSName = req.OS_Name;
            Arch = req.Arch;
            Event = (EventType) req.Event;
            Extra = req.Extra;
        }
    }
}