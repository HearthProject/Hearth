﻿using LiteDB;
using Newtonsoft.Json;

namespace Hearth.Models
{
    public class DatabaseObject
    {
        [JsonIgnore]
        [BsonId]
        public ObjectId DatabaseId { get; set; }
    }
}