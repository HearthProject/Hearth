﻿using Hearth.Models.User;
using Newtonsoft.Json;

namespace Hearth.Models
{
    public class Log : DatabaseObject
    {
        [JsonProperty("notes")]
        public string Notes { get; set; }
        
        [JsonProperty("gameLog")]
        public string GameLog { get; set; }
        
        [JsonProperty("owner")]
        public PublicUser Owner { get; set; }
        
        [JsonProperty("timestamp")]
        public long Timestamp { get; set; }
        
        [JsonProperty("id")]
        public string Id { get; set; }
    }
}