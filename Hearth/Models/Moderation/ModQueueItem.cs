﻿using Newtonsoft.Json;

namespace Hearth.Models.Moderation
{
    public enum ModerationEventType
    {
        NewPack,
        NewMod,
        PackFlagged
    }
    
    public class ModQueueItem : DatabaseObject
    {
        [JsonProperty("type")]
        public ModerationEventType EventType { get; set; }
        
        [JsonProperty("itemId")]
        public string ItemId { get; set; }
        
        [JsonProperty("notes")]
        public string Notes { get; set; }
        
        [JsonProperty("closed")]
        public bool Closed { get; set; }
    }
}