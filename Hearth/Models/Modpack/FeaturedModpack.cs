﻿using System;
using Newtonsoft.Json;

namespace Hearth.Models.Modpack
{
    public class FeaturedModpack : DatabaseObject
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        
        [JsonProperty("sourceType")]
        public BaseSource SourceType { get; set; }
        
        [JsonProperty("notes")]
        public string Notes { get; set; }
        
        [JsonProperty("timestamp")]
        public long Timestamp { get; set; }
    }
}