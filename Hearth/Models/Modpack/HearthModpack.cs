﻿using System.Collections.Generic;
using Hearth.Models.User;
using Newtonsoft.Json;

namespace Hearth.Models.Modpack
{
    public class HearthModpack : DatabaseObject
    {
        [JsonProperty("info")]
        public ModpackInfo Info { get; set; }
        
        [JsonProperty("minecraft")]
        public LoaderInfo Minecraft { get; set; }
        
        [JsonProperty("forge")]
        public LoaderInfo Forge { get; set; }
        
        [JsonProperty("mods")]
        public List<PackMod> Mods { get; set; }
        
        [JsonProperty("files")]
        public List<ModpackFile> Files { get; set; }
        
        [JsonProperty("public")]
        public bool IsPublic { get; set; }
        
        [JsonProperty("authorizedUsers")]
        public List<PublicUser> AuthorizedUsers { get; set; }
        
        [JsonProperty("owner")]
        public PublicUser Owner { get; set; }
    }

    public class ModpackInfo
    {
        [JsonProperty("title")]
        public string Title { get; set; }
        
        [JsonProperty("desc")]
        public string Desc { get; set; }
        
        [JsonProperty("version")]
        public int Version { get; set; }
        
        [JsonProperty("id")]
        public string Id { get; set; }
        
        [JsonProperty("icon")]
        public IconFile Icon { get; set; }
    }

    public class IconFile
    {
        [JsonProperty("file")]
        public string File { get; set; }
        
        [JsonProperty("hash")]
        public string Hash { get; set; }
    }
    
    public class ModpackFile
    {
        [JsonProperty("source")]
        public string Source { get; set; }
        
        [JsonProperty("dest")]
        public string Dest { get; set; }
        
        [JsonProperty("hash")]
        public string Hash { get; set; }
    }

    public class LoaderInfo
    {
        [JsonProperty("version")]
        public string Version { get; set; }
    }

    public class PackMod
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        
        [JsonProperty("source")]
        public int Source { get; set; }
        
        [JsonProperty("file")]
        public string File { get; set; }
        
        [JsonProperty("project")]
        public int Project { get; set; }
        
        [JsonProperty("sha1")]
        public string Sha1 { get; set; }
    }
}