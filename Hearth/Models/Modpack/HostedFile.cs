﻿using Newtonsoft.Json;

namespace Hearth.Models.Modpack
{
    public class HostedFile : DatabaseObject
    {
        [JsonProperty("hash")]
        public string hash { get; set; }
        
        [JsonProperty("storedFilename")]
        public string storedFileName { get; set; }
        
        [JsonProperty("filename")]
        public string fileName { get; set; }
        
        [JsonProperty("size")]
        public long size { get; set; }

        [JsonProperty("ownerFiles")]
        public string[] ownerFiles { get; set; }
    }
}
