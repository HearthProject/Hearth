﻿using Newtonsoft.Json;

namespace Hearth.Models
{
    public class OneMetaSource
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        
        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("hashUrl")]
        public string HashUrl { get; set; } = string.Empty;
        
        [JsonProperty("source")]
        public BaseSource Source { get; set; }
        
        [JsonProperty("compression")]
        public CompressionType CompressionType { get; set; }
    }

    public enum BaseSource
    {
        Hearth,
        Curse,
        Atlauncher,
        Technic
    }

    public enum CompressionType
    {
        None,
        Zip,
        Xz
    }
}