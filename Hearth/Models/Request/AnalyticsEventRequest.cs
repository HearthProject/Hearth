﻿namespace Hearth.Models.Request
{
    public class AnalyticsEventRequest
    {
        public string UUID { get; set; }
        
        public string Version { get; set; }
        
        public string Builder { get; set; }
        
        public string OS_Name { get; set; }
        
        public string Arch { get; set; }
        
        public int Mem { get; set; }
        
        public string Locale { get; set; }
        
        public int Event { get; set; }
        
        public string Extra { get; set; }
    }
}