﻿namespace Hearth.Models.Request
{
    public class InviteKeyGenerationRequest
    {
        public int KeyAmount { get; set; } = 1;
    }
}