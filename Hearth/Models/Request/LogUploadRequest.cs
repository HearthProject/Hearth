﻿using Hearth.Models.User;

namespace Hearth.Models.Request
{
    public class LogUploadRequest
    {
        public string Notes { get; set; }
        
        public string GameLog { get; set; }

        public RegisteredUser Owner { get; set; } = null;
    }
}