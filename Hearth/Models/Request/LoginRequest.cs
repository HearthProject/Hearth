﻿namespace Hearth.Models.Request
{
    public class LoginRequest
    {
        public string LoginHash { get; set; }
        
        public string Username { get; set; }

        public string InviteKey { get; set; } = string.Empty;

        public string UserIP { get; set; } = string.Empty;
    }
}