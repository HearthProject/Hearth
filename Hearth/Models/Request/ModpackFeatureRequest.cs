﻿namespace Hearth.Models.Request
{
    public class ModpackFeatureRequest
    {
        public BaseSource SourceType { get; set; }
        
        public int Id { get; set; }
        
        public string Notes { get; set; }
    }
}