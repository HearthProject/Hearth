﻿using Newtonsoft.Json;

namespace Hearth.Models.Response
{
    public enum ModpackError
    {
        NoFile,
        OutOfStorage,
        MaliciousFile,
        TooSlow
    }
    
    public class InvalidModpackResponse
    {
        [JsonProperty("error")]
        public ModpackError Error { get; set; }
        
        [JsonProperty("errorMessage")]
        public string ErrorMessage { get; set; }

        public InvalidModpackResponse(ModpackError err)
        {
            Error = err;
            ErrorMessage = new[]{"No file", "Out of storage", "Malicious file", "Too slow"}[(int) Error];
        }
    }
}