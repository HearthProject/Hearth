﻿using Newtonsoft.Json;

namespace Hearth.Models.Response
{
    public class ProfileResponse
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        
        [JsonProperty("id")]
        public string UUID { get; set; }
    }
}