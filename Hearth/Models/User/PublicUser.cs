﻿using Newtonsoft.Json;

namespace Hearth.Models.User
{
    public class PublicUser
    {
        [JsonProperty("uuid")]
        public string UUID { get; set; }
        
        [JsonProperty("username")]
        public string Username { get; set; }
        
        [JsonProperty("role")]
        public UserRole Role { get; set; }
        
        public PublicUser(RegisteredUser u)
        {
            UUID = u.UUID;
            Username = u.Username;
            Role = u.Role;
        }
    }
}