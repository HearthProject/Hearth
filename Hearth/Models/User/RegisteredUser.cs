﻿using Newtonsoft.Json;

namespace Hearth.Models.User
{
    public enum UserRole
    {
        Guest,
        User,
        Trusted,
        Moderator,
        Admin
    }
    
    public class RegisteredUser : DatabaseObject
    {
        [JsonProperty("uuid")]
        public string UUID { get; set; }
        
        [JsonProperty("username")]
        public string Username { get; set; }
        
        [JsonProperty("apikey")]
        public string ApiKey { get; set; }

        [JsonProperty("role")]
        public UserRole Role { get; set; } = UserRole.Guest;

        [JsonProperty("banned")]
        public bool Banned { get; set; }
        
        [JsonProperty("usedStorage")]
        public long UsedStorage { get; set; }

        [JsonIgnore]
        public string UsedKey { get; set; } = string.Empty;

        [JsonIgnore]
        public string[] UsedIps { get; set; } = { };
    }
}