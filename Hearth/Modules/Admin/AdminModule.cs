﻿using Hearth.Configuration;
using Hearth.Models.Request;
using Hearth.Models.User;
using Hearth.Utils;
using Nancy;
using Nancy.ModelBinding;

namespace Hearth.Modules.Admin
{
    public class AdminModule : AuthenticatedModule
    {
        public AdminModule() : base("/admin")
        {
            MinRole = UserRole.Admin;
            
            Get("/invites", _ => Response.AsJsonNet(HearthContext.UserManager.InviteKeys));
            
            Put("/invites", async _ =>
            {
                var req = this.Bind<InviteKeyGenerationRequest>();
                var keys = await HearthContext.UserManager.GenerateKeysAsync(req.KeyAmount);
                return Response.AsJsonNet(keys);
            });
            
            Post("/feature", async _ =>
            {
                var req = this.Bind<ModpackFeatureRequest>();
                var obj = await HearthContext.FeaturedModpacks.InsertFeaturedAsync(req);
                return Response.AsJsonNet(obj);
            });
        }
    }
}