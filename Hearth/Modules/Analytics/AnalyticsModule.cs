﻿using Hearth.Configuration;
using Hearth.Models.Analytics;
using Hearth.Models.Request;
using Nancy;
using Nancy.ModelBinding;

namespace Hearth.Modules.Analytics
{
    public class AnalyticsModule : NancyModule
    {
        public AnalyticsModule() : base("/analytics")
        {
            Post("/event", async _ =>
            {
                var req = this.Bind<AnalyticsEventRequest>();
                var ap = new AnalyticsEvent(req);
                ap.Addr = Request.Headers["X-Real-IP"].ToString();
                await HearthContext.AnalyticsDB.StoreEventAsync(ap);
                return HttpStatusCode.Created;
            });
        }
    }
}