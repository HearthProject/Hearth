﻿using System.Linq;
using Hearth.Configuration;
using Hearth.Models.User;
using Hearth.Services.Auth;
using Nancy;

namespace Hearth.Modules
{
    public abstract class AuthenticatedModule : NancyModule
    {
        protected RegisteredUser CurrentUser { get; set; }
        protected UserRole MinRole { get; set; } = UserRole.Guest;

        public AuthenticatedModule(string path) : base(path)
        {
            Before += ctx =>
            {
                if (ctx.CurrentUser == null) return HttpStatusCode.Unauthorized;
                var userIdentifier = Context.CurrentUser.Claims.FirstOrDefault(x => x.Type == "user_id").Value;
                
                CurrentUser = HearthContext.UserManager.FindUserByUUIDAsync(userIdentifier).Result;
                if (!CurrentUser.UsedIps.Contains(ctx.Request.Headers["X-Real-IP"].ToString()))
                {
                    CurrentUser.UsedIps.Append(ctx.Request.Headers["X-Real-IP"].ToString());
                    HearthContext.UserManager.UpdateUserAsync(CurrentUser);
                }
                if (CurrentUser.Banned) return HttpStatusCode.Forbidden;
                if (CurrentUser.Role < MinRole) return HttpStatusCode.Forbidden;
                return null;
            };
        }
    }
}