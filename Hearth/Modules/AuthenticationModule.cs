﻿using System;
using System.Security;
using Hearth.Configuration;
using Hearth.Models.Request;
using Hearth.Utils;
using Nancy;
using Nancy.ModelBinding;

namespace Hearth.Modules
{
    public class AuthenticationModule : NancyModule
    {
        public AuthenticationModule() : base("/auth")
        {
            Before += ctx =>
            {
                return null;
            };
            
            Post("/login", async args =>
            {
                var req = this.Bind<LoginRequest>();
                req.UserIP = "127.0.0.1";
                try
                {
                    var user = await HearthContext.UserManager.LoginUserAsync(req);
                    return Response.AsJsonNet(user);
                }
                catch (SecurityException sx)
                {
                    return Response.AsText(sx.Message).WithStatusCode(HttpStatusCode.Unauthorized);
                }
            });
        }
    }
}