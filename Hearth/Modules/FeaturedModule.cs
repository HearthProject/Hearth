﻿using Hearth.Configuration;
using Hearth.Utils;
using Nancy;

namespace Hearth.Modules
{
    public class FeaturedModule : NancyModule
    {
        public FeaturedModule()
        {
            Get("/featured", async _ => Response.AsJsonNet(await HearthContext.FeaturedModpacks.GetFeaturedModpacksAsync()));
        }
    }
}