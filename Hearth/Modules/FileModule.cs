﻿using System;
using Hearth.Configuration;
using Nancy;

namespace Hearth.Modules
{
    public class FileModule : NancyModule
    {
        public FileModule() : base("/file")
        {
            Get("/{hash}", async args =>
            {
                var fileDat = await HearthContext.FileStorage.FindByHashAsync(args.hash);
                if (fileDat == null) return HttpStatusCode.NotFound;
                var filePath = HearthContext.HearthConfig.StorageUrl;
                if (!filePath.EndsWith("/")) filePath += "/";
                filePath += fileDat.Hash;
                return Response.AsRedirect(filePath);
            });
        }
    }
}