﻿using Hearth.Configuration;
using Hearth.Utils;
using Nancy;

namespace Hearth.Modules
{
    public class HomeModule : NancyModule
    {
        public HomeModule()
        {
            Get("/", _ => Response.AsJsonNet(new PublicHearthConfig(HearthContext.HearthConfig)));
        }
    }
}