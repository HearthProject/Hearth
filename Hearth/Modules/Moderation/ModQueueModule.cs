﻿using Hearth.Models.User;
using Hearth.Services.Moderation;
using Hearth.Services.Webhook;
using Hearth.Utils;
using Nancy;

namespace Hearth.Modules.Moderation
{
    public class ModQueueModule : AuthenticatedModule
    {
        private ModerationQueue ModQueue { get; }
        private DiscordWebhook Webhook { get; }
        
        public ModQueueModule() : base("/modqueue")
        {
            ModQueue = new ModerationQueue();
            Webhook = new DiscordWebhook();
            MinRole = UserRole.Moderator;
            
            Get("/", _ => Response.AsJsonNet(ModQueue.GetOpenEventsAsync().Result));
        }
    }
}