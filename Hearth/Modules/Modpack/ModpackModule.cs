﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;
using Hearth.Configuration;
using Hearth.Models.Modpack;
using Hearth.Models.Response;
using Hearth.Models.User;
using Hearth.Utils;
using Nancy;
using Newtonsoft.Json;

namespace Hearth.Modules.Modpack
{
    public class ModpackModule : AuthenticatedModule {
        public ModpackModule() : base("/modpack") {
            Post("/create", async _ =>
            {

            });

            Post("/<pid>/upload", async args => {
                if (Request.Files.ToArray().Length != 1) return Response.AsJsonNet(new InvalidModpackResponse(ModpackError.NoFile))
                    .WithStatusCode(HttpStatusCode.BadRequest);
                
                // 1048576 Bytes = 1 Mebibyte
                var maxSize = HearthContext.HearthConfig.MaxFileSize[(int) CurrentUser.Role] * 1048576;
                var maxTotalSize = HearthContext.HearthConfig.MaxTotalSize[(int) CurrentUser.Role] * 1048576;
                
                var f = Request.Files.First();
                if (f.Value.Length > maxSize ||
                    CurrentUser.UsedStorage + f.Value.Length > maxTotalSize) 
                    return Response.AsJsonNet(new InvalidModpackResponse(ModpackError.OutOfStorage)).WithStatusCode(HttpStatusCode.BadRequest);

                var fileId = StringUtils.SecureRandomString(32);
                
                var tempFolder = Path.GetFullPath(Path.Combine(new []{
                        HearthContext.HearthConfig.CacheDir,
                        CurrentUser.UUID,
                        "TEMP_UPLOADING"
                }));
                var tempFile = Path.Combine(tempFolder, fileId + ".1f");

                
                var extractedTempFolder = Path.Combine(tempFolder, fileId);
                Directory.CreateDirectory(tempFolder);
                Console.WriteLine("Uploading to " + tempFile);
                using (var fileStream = new FileStream(tempFile, FileMode.Create))
                {
                    await f.Value.CopyToAsync(fileStream);
                }
            
                if (!string.IsNullOrWhiteSpace(HearthContext.HearthConfig.FireGuardCommand))
                {
                    var fgCommand = HearthContext.HearthConfig.FireGuardCommand.Replace("{FILE}", tempFile).Split(" ");
                    var fgBin = fgCommand[0];
                    var fgArgs = string.Join(" ", fgCommand.Skip(1));
                    var p = new Process
                    {
                        StartInfo =
                        {
                            UseShellExecute = false,
                            RedirectStandardOutput = true,
                            FileName = fgBin,
                            Arguments = fgArgs
                        }
                    };
                    p.Start();
                    var cmdOut = await p.StandardOutput.ReadToEndAsync();
                    p.WaitForExit();
                    if (p.ExitCode != 0)
                    {
                        Console.WriteLine(cmdOut);
                        // FireGuard caught something suspicious
                        // TODO: Something about it
                        File.Delete(tempFile);
                        return Response.AsJsonNet(new InvalidModpackResponse(ModpackError.MaliciousFile))
                            .WithStatusCode(HttpStatusCode.BadRequest);
                    }
                }

                ZipFile.ExtractToDirectory(tempFile, extractedTempFolder);
                File.Delete(tempFile);

                if (!File.Exists(Path.Combine(extractedTempFolder, "manifest.json")))
                {
                    Directory.Delete(extractedTempFolder, true);
                    return HttpStatusCode.BadRequest;
                }

                var modpackManifest =
                    JsonConvert.DeserializeObject<HearthModpack>(
                        File.ReadAllText(Path.Combine(extractedTempFolder, "manifest.json")));

                var cleanedManifest = new HearthModpack
                {
                    Info = modpackManifest.Info,
                    Minecraft = modpackManifest.Minecraft,
                    Forge = modpackManifest.Forge,
                    Mods = modpackManifest.Mods,
                    Owner = new PublicUser(CurrentUser),
                    Files = new List<ModpackFile>()
                };

                cleanedManifest.Info.Id = args.pid;
                
                foreach (var file in modpackManifest.Files)
                {
                    var absPath = Path.GetFullPath(Path.Combine(extractedTempFolder, file.Source));
                    if (!absPath.StartsWith(extractedTempFolder))
                    {
                        Console.WriteLine(CurrentUser.Username + " attempted to perform a directory traversal. Banning");
                        Directory.Delete(extractedTempFolder, true);
                        return Response.AsJsonNet(new InvalidModpackResponse(ModpackError.MaliciousFile))
                            .WithStatusCode(HttpStatusCode.BadRequest);
                    }

                    var storedFile = await HearthContext.FileStorage.saveFileAsync(absPath, fileId);
                    cleanedManifest.Files.Add(new ModpackFile
                    {
                        Dest = file.Dest,
                        Hash = storedFile.hash,
                        Source = HearthContext.HearthConfig.BaseUrl + "/file/" + storedFile.hash
                    });
                }
                
                return Response.AsJsonNet(cleanedManifest);
            });
        }
    }
}
