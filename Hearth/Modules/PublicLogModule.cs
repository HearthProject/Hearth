﻿using Hearth.Configuration;
using Hearth.Models;
using Hearth.Utils;
using Nancy;

namespace Hearth.Modules
{
    public class PublicLogModule : NancyModule
    {
        public PublicLogModule() : base("/logs")
        {
            Get("/{id}", async args =>
            {
                Log log = await HearthContext.LogStorage.GetLogById(args.id);
                if (log == null) return HttpStatusCode.NotFound;
                return Response.AsJsonNet(log);
            });
        }
    }
}