﻿using Hearth.Configuration;
using Hearth.Models.Request;
using Hearth.Utils;
using Nancy.ModelBinding;

namespace Hearth.Modules.User
{
    public class LogModule : AuthenticatedModule
    {
        public LogModule() : base("/logs")
        {
            Put("/upload", async _ =>
            {
                var req = this.Bind<LogUploadRequest>();
                req.Owner = CurrentUser;
                var nl = await HearthContext.LogStorage.UploadLogAsync(req);
                return Response.AsJsonNet(nl);
            });

            Get("/mine", async _ => Response.AsJsonNet(await HearthContext.LogStorage.GetAllLogsForUser(CurrentUser)));
        }
    }
}