﻿using System;
using Hearth.Models.User;
using Hearth.Services.Auth;
using Hearth.Utils;
using Nancy;
using Newtonsoft.Json;

namespace Hearth.Modules.User
{
    public class PublicUserModule : NancyModule
    {
        private UserManagerService UserManager { get; }
        
        public PublicUserModule() : base("/user")
        {
            UserManager = new UserManagerService();
            
            Get("/{user}", async args =>
            {
                RegisteredUser u = await UserManager.FindUserByUsernameAsync(args.user);
                if (u == null)
                {
                    u = await UserManager.FindUserByUUIDAsync(args.user);
                    if (u == null) return HttpStatusCode.NotFound;
                }
                if (u.Banned) return HttpStatusCode.NotFound;
                return Response.AsJsonNet(new PublicUser(u));
            });
        }
    }
}