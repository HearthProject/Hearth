﻿using Nancy;
using Newtonsoft.Json;

namespace Hearth.Modules.User
{
    public class UserHome : AuthenticatedModule
    {
        public UserHome() : base("/user")
        {
            Get("/", _ => Response.AsText(JsonConvert.SerializeObject(CurrentUser)).WithContentType("application/json"));
        }
    }
}