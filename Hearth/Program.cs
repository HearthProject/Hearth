﻿using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;

namespace Hearth
{
    class Program
    {
        static void Main(string[] args)
        {
            var appDirectory = Directory.GetCurrentDirectory();
            var config = new ConfigurationBuilder()
                .SetBasePath(appDirectory)
                .AddCommandLine(args)
                .AddJsonFile(Path.Combine(appDirectory, "hosting.json"), true)
                .Build();

            var host = new WebHostBuilder()
                .UseKestrel()
                .UseConfiguration(config)
                .UseContentRoot(appDirectory)
                .UseStartup<Startup>()
                .Build();
            host.Run();
        }
    }
}