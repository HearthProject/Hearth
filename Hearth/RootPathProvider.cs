﻿using System.IO;
using Nancy;

namespace Hearth
{
    public class RootPathProvider : IRootPathProvider
    {
        public string GetRootPath() => Directory.GetCurrentDirectory();
    }
}