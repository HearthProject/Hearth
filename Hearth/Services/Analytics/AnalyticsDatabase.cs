﻿using System.Threading.Tasks;
using Hearth.Configuration;
using Hearth.Models.Analytics;
using LiteDB;

namespace Hearth.Services.Analytics
{
    public class AnalyticsDatabase
    {
        private readonly LiteCollection<AnalyticsEvent> _analyticsCollection;

        public AnalyticsDatabase()
        {
            _analyticsCollection = HearthContext.Database.GetCollection<AnalyticsEvent>("a_events");
        }

        public async Task<bool> StoreEventAsync(AnalyticsEvent evt)
        {
            return await Task.Run(() =>
            {
                _analyticsCollection.Insert(evt);
                return true;
            });
        }
    }
}