﻿using System.Collections.Generic;
using System.Net;
using System.Security;
using Hearth.Models.Request;
using Hearth.Models.User;
using LiteDB;
using System.Threading.Tasks;
using Hearth.Configuration;
using Hearth.Models.Response;
using Hearth.Utils;
using Newtonsoft.Json;
using RestSharp;

namespace Hearth.Services.Auth
{
    public class UserManagerService
    {
        private readonly LiteCollection<RegisteredUser> _userCollection;
        public readonly List<string> InviteKeys = new List<string>();

        public UserManagerService()
        {
            _userCollection = HearthContext.Database.GetCollection<RegisteredUser>("r_users");
        }

        public async Task<RegisteredUser> LoginUserAsync(LoginRequest req)
        {
            return await Task.Run(async () =>
            {
                var client = new RestClient("https://sessionserver.mojang.com");
                var request = new RestRequest("session/minecraft/hasJoined", Method.GET);
                request.AddParameter("username", req.Username, ParameterType.QueryString);
                request.AddParameter("ip", req.UserIP, ParameterType.QueryString);
                request.AddParameter("serverId", req.LoginHash, ParameterType.QueryString);

                var response = client.Execute(request);
                if (response.StatusCode != HttpStatusCode.OK) throw new SecurityException("Invalid Credentials!");
                
                var profile = JsonConvert.DeserializeObject<ProfileResponse>(response.Content);
                
                var foundUser = await FindUserByUUIDAsync(profile.UUID);
                if (foundUser != null) return foundUser;
                if (HearthContext.HearthConfig.InviteRequired)
                {
                    if (string.IsNullOrWhiteSpace(HearthContext.HearthConfig.InitialAdmin) ||
                        profile.UUID != HearthContext.HearthConfig.InitialAdmin)
                    {
                        if (string.IsNullOrWhiteSpace(req.InviteKey) || !InviteKeys.Remove(req.InviteKey))
                        {
                            throw new SecurityException("Invalid Invite Key");
                        }
                    }
                }
                
                var newRole = UserRole.Guest;
                if (!string.IsNullOrWhiteSpace(HearthContext.HearthConfig.InitialAdmin) && profile.UUID == HearthContext.HearthConfig.InitialAdmin)
                    newRole = UserRole.Admin;

                var newUser = new RegisteredUser
                {
                    ApiKey = StringUtils.SecureRandomString(32),
                    Username = profile.Name,
                    UUID = profile.UUID,
                    Role = newRole,
                    UsedKey = req.InviteKey
                };

                _userCollection.Insert(newUser);

                _userCollection.EnsureIndex(x => x.UUID);
                _userCollection.EnsureIndex(x => x.ApiKey);
                _userCollection.EnsureIndex(x => x.Username);
                
                return newUser;
            });
        }

        public async Task<RegisteredUser> FindUserByApiKeyAsync(string apikey)
        {
            return await Task.Run(() => _userCollection.FindOne(x => x.ApiKey == apikey));
        }

        public async Task<RegisteredUser> FindUserByUUIDAsync(string UUID)
        {
            return await Task.Run(() => _userCollection.FindOne(x => x.UUID == UUID));
        }

        public async Task<RegisteredUser> FindUserByUsernameAsync(string username)
        {
            return await Task.Run(() => _userCollection.FindOne(x => x.Username == username));
        }

        public async Task<List<string>> GenerateKeysAsync(int amount)
        {
            return await Task.Run(() =>
            {
                var newKeys = new List<string>();

                for (var i = 0; i < amount; i++)
                {
                    newKeys.Add(StringUtils.SecureRandomString(32));
                }
                
                InviteKeys.AddRange(newKeys);
                
                return newKeys;
            });
        }

        public async Task UpdateUserAsync(RegisteredUser user)
        {
            var lockEntry = HearthContext.UserServiceTable.GetOrCreate(user.UUID).UserLock;
            await lockEntry.WithExclusiveWriteAsync(Task.Run(async () =>
            {
                return Task.Run(() => _userCollection.Update(user)).Result;
            }));
        }

        public int GetUserCount()
        {
            return _userCollection.Count();
        }
    }
}
