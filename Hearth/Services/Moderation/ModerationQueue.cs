﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hearth.Configuration;
using Hearth.Models.Moderation;
using LiteDB;

namespace Hearth.Services.Moderation
{
    public class ModerationQueue
    {
        private LiteCollection<ModQueueItem> _itemCollection;

        public ModerationQueue()
        {
            _itemCollection = HearthContext.Database.GetCollection<ModQueueItem>("m_queue");
        }

        public async Task<List<ModQueueItem>> GetOpenEventsAsync()
        {
            return await Task.Run(() => _itemCollection.FindAll().Where(x => !x.Closed).ToList());
        }
    }
}