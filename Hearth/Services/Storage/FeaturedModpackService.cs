﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hearth.Configuration;
using Hearth.Models.Modpack;
using Hearth.Models.Request;
using LiteDB;

namespace Hearth.Services.Storage
{
    public class FeaturedModpackService
    {
        private readonly LiteCollection<FeaturedModpack> _featuredCollection;

        public FeaturedModpackService()
        {
            _featuredCollection = HearthContext.Database.GetCollection<FeaturedModpack>("f_packs");
        }

        public async Task<FeaturedModpack> InsertFeaturedAsync(ModpackFeatureRequest req)
        {
            return await Task.Run(() =>
            {
                var newFeatured = new FeaturedModpack
                {
                    Id = req.Id,
                    SourceType = req.SourceType,
                    Notes = req.Notes,
                    Timestamp = DateTimeOffset.UtcNow.ToUnixTimeSeconds()
                };

                _featuredCollection.Insert(newFeatured);

                return newFeatured;
            });
        }

        public async Task<List<FeaturedModpack>> GetFeaturedModpacksAsync()
        {
            return await Task.Run(() => _featuredCollection.FindAll().ToList());
        }
    }
}