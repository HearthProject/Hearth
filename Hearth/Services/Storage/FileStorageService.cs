﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Hearth.Configuration;
using Hearth.Models.Modpack;
using Hearth.Utils;
using LiteDB;

namespace Hearth.Services.Storage
{
    public class FileStorageService
    {
        private readonly LiteCollection<HostedFile> _fileCollection;
        private readonly string _basePath;

        public FileStorageService()
        {
            _fileCollection = HearthContext.Database.GetCollection<HostedFile>("h_files");
            _basePath = HearthContext.HearthConfig.DataDir;
        }

        public async Task<HostedFile> saveFileAsync(string filename, string ownerFile)
        {
            return await Task.Run(async () =>
            {
                string fileHash;
                using (var stream = File.OpenRead(filename))
                {
                    var sha = new SHA1Managed();
                    var hash = sha.ComputeHash(stream);
                    fileHash = BitConverter.ToString(hash).Replace("-", string.Empty);
                }

                var foundFile = await FindByHashAsync(fileHash);

                if (foundFile != null) {
                    foundFile.ownerFiles.Append(ownerFile);
                    _fileCollection.Update(foundFile);
                    return foundFile;
                }

                var file = new HostedFile
                {
                    hash = fileHash,
                    fileName = new FileInfo(filename).Name,
                    size = new FileInfo(filename).Length
                };
                
                File.Copy(filename, Path.Combine(_basePath, file.hash));

                file.ownerFiles.Append(ownerFile);
                _fileCollection.Insert(file);

                _fileCollection.EnsureIndex(x => x.hash);
                _fileCollection.EnsureIndex(x => x.storedFileName);
                
                return file;
            });
        }

        public async Task<HostedFile> FindByHashAsync(string hash)
        {
            return await Task.Run(() => _fileCollection.FindOne(x => x.hash == hash));
        }
    }
}
