﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hearth.Configuration;
using Hearth.Models;
using Hearth.Models.Request;
using Hearth.Models.User;
using Hearth.Utils;
using LiteDB;

namespace Hearth.Services.Storage
{
    public class LogStorageService
    {
        private readonly LiteCollection<Log> _logCollection;

        public LogStorageService()
        {
            _logCollection = HearthContext.Database.GetCollection<Log>("logs");
        }

        public async Task<Log> UploadLogAsync(LogUploadRequest req)
        {
            return await Task.Run(() =>
            {
                var newLog = new Log
                {
                    GameLog = req.GameLog,
                    Notes = req.Notes,
                    Owner = new PublicUser(req.Owner),
                    Timestamp = DateTimeOffset.UtcNow.ToUnixTimeSeconds(),
                    Id = StringUtils.SecureRandomString(32)
                };

                _logCollection.Insert(newLog);

                _logCollection.EnsureIndex(x => x.Id);

                return newLog;
            });
        }

        public async Task<Log> GetLogById(string id)
        {
            return await Task.Run(() => _logCollection.FindOne(x => x.Id == id));
        }

        public async Task<List<Log>> GetAllLogsForUser(RegisteredUser user)
        {
            return await Task.Run(() => _logCollection.FindAll().Where(x => x.Owner.UUID == user.UUID).ToList());
        }
    }
}