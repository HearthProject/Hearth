﻿using System;
using System.Threading.Tasks;
using Hearth.Configuration;
using RestSharp;

namespace Hearth.Services.Webhook
{
    public class DiscordWebhook
    {
        private RestClient Client;

        private string HookPath { get; }
        
        public DiscordWebhook()
        {
            Client = new RestClient("https://discordapp.com");
            HookPath = HearthContext.HearthConfig.WebhookPath;
        }

        public async void SendMessage(string message)
        {
            await Task.Run(() =>
            {
                if (string.IsNullOrWhiteSpace(HookPath)) return;
                var request = new RestRequest(HookPath, Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddJsonBody(
                    new
                    {
                        username = "Hearth AutoModerator",
                        content = message
                    });
                Client.Execute(request);
            });
        }
    }
}