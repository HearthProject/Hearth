﻿using System;
using System.IO;
using Hearth.Configuration;
using Hearth.Infrastructure.Concurrency;
using Hearth.Services.Analytics;
using Hearth.Services.Auth;
using Hearth.Services.Storage;
using LiteDB;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Nancy.Owin;
using Newtonsoft.Json;

namespace Hearth
{
    public class Startup
    {
        public void Configure(IApplicationBuilder app)
        {
            app.UseOwin(x => x.UseNancy());
        }

        public Startup(IHostingEnvironment env)
        {
            Console.WriteLine("Starting Hearth Server");
            if (!File.Exists("config.json"))
            {
                Console.WriteLine("Config file doesn't exist, writing default");
                var confFileContents = JsonConvert.SerializeObject(new HearthConfiguration(), Formatting.Indented);
                File.WriteAllText("config.json", confFileContents);
            }
            Console.WriteLine("Loading configuration");
            HearthContext.HearthConfig =
                JsonConvert.DeserializeObject<HearthConfiguration>(File.ReadAllText("config.json"));
            var settingsPassed = true;
            if (string.IsNullOrWhiteSpace(HearthContext.HearthConfig.StorageUrl))
            {
                Console.WriteLine("storageUrl is not set!");
                Console.WriteLine("Please set storageUrl in config.json and restart the program.");
                settingsPassed = false;
            }
            if (string.IsNullOrWhiteSpace(HearthContext.HearthConfig.BaseUrl))
            {
                Console.WriteLine("baseUrl is not set!");
                Console.WriteLine("Please set baseUrl in config.json and restart the program.");
                settingsPassed = false;
            }
            if (!settingsPassed) 
                Environment.Exit(1);
            Console.WriteLine("Loading database");
            HearthContext.Database = new LiteDatabase(HearthContext.HearthConfig.DatabaseFile);
            Console.WriteLine("Loading services");
            HearthContext.UserManager = new UserManagerService();
            HearthContext.AnalyticsDB = new AnalyticsDatabase();
            HearthContext.FileStorage = new FileStorageService();
            HearthContext.UserServiceTable = new UserServiceTable();
            HearthContext.FeaturedModpacks = new FeaturedModpackService();
            HearthContext.LogStorage = new LogStorageService();
            Console.WriteLine("Creating files");
            Directory.CreateDirectory(HearthContext.HearthConfig.CacheDir);
            Directory.CreateDirectory(HearthContext.HearthConfig.DataDir);
        }
    }
}