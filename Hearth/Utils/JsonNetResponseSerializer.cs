﻿using Nancy;
using Newtonsoft.Json;

namespace Hearth.Utils
{
    public static class JsonNetResponseSerializer
    {
        public static Response AsJsonNet<T>(this IResponseFormatter formatter, T instance)
        {
            var responseData = JsonConvert.SerializeObject(instance);
            return formatter.AsText(responseData, "application/json");
        }
    }
}