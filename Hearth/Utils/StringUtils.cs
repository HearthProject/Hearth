﻿using System.Security.Cryptography;
using System.Text;

namespace Hearth.Utils
{
    public class StringUtils
    {
        public static string SecureRandomString(int maxSize)
        {
            var chars = new char[62];
            chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
            var data = new byte[1];
            using (var prng = RandomNumberGenerator.Create())
            {
                prng.GetBytes(data);
                data = new byte[maxSize];
                prng.GetBytes(data);
            }
            var result = new StringBuilder(maxSize);
            foreach (var b in data) result.Append(chars[b % chars.Length]);
            return result.ToString();
        }
    }
}