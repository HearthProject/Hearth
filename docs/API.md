# API Documentation for Hearth

REST API route descriptions:

- Info
    - Server Info
- Auth
    - Login
- User
    - Current User
    - User Info
    - Ban User
    - Delete User
- Packs
    - List Packs
    - Create Pack
    - Add User
- Moderation
    - View Modqueue
- Admin
    - View Invites
    - Generate Invites

## Info

### Server Info

> View the server configuration

`GET /`

Minimum Role: None

RESPONSE:

```json
{
    "maxFileSize": [
        <FILE SIZE FOR EACH ROLE>
    ],
    "maxTotalSize": [
        <MAX TOTAL SIZE FOR EACH ROLE>
    ],
    "inviteRequired": <INVITATION REQUIRED>,
    "instanceName": "<NAME OF HEARTH INSTANCE>",
    "roleNames": [
        "<DISPLAY NAME FOR EACH ROLE>"
    }
}
```

## Auth

### Login

> Log in using a Mojang account

`POST /auth/login`

Minimum Role: None

PART 1:

`POST https://sessionserver.mojang.com/session/minecraft/join`

```json
{
    "accessToken": "<MOJANG ACCESS TOKEN>",
    "selectedProfile": "<ACCOUNT UUID>",
    "serverId": "<RANDOM HASH FOR SESSION>"
}
```

PART 2:

REQUEST:

```json
{
    "loginHash": "<HASH FROM PART 1>",
    "username": "<MINECRAFT ACCOUNT USERNAME>",
    "inviteKey": "<INVITATION KEY>"
}
```

RESPONSE:

```json
{
    "uuid": "<MINECRAFT ACCOUNT UUID>",
    "username": "<MINECRAFT USERNAME>",
    "apikey": "<HEARTH API KEY>",
    "role": <HEARTH ACCOUNT ROLE>,
    "banned": <ACCOUNT BANNED>,
    "usedStorage": <MEGABYTES USED>,
    "maxFileSize": <MAXIMUM SINGLE FILE SIZE>,
    "maxTotalSize": <MAXIMUM TOTAL STORAGE>
}
```

## User

### Current User

> Get info about logged in user

`GET /user`

Minimum Role: Guest

RESPONSE:

```json
{
    "uuid": "<MINECRAFT ACCOUNT UUID>",
    "username": "<MINECRAFT USERNAME>",
    "apikey": "<HEARTH API KEY>",
    "role": <HEARTH ACCOUNT ROLE>,
    "banned": <ACCOUNT BANNED>,
    "usedStorage": <MEGABYTES USED>
}
```

### User Info

> Get info about a registered user

`GET /user/<UUID OR USERNAME>`

Minimum Role: None

RESPONSE:

```json
{
    "uuid": "<MINECRAFT ACCOUNT UUID>",
    "username": "<MINECRAFT USERNAME>",
    "role": <HEARTH ACCOUNT ROLE>
}
```

### Ban User

> Do I really have to explain this

`PUT /user/<UUID OR USERNAME>/banned`

Minimum Role: Moderator

```json
{
    "banned": <BAN OR UNBAN>,
    "reason": "<MESSAGE EXPLAINING BAN>"
}
```

### Delete User

> Completely delets a user

`DELETE /user/<UUID OR USERNAME>`

Minimum Role: Admin

## Packs

### List Packs

> List all packs availible to the current user

`GET /modpack/list`

Minimum Role: None

RESPONSE:

```json
[
    {
        "id": <ID OF PACK>,
        "title": <TITLE OF PACK>,
        "url": "<URL FOR PACK INFO>",
        "downloadUrls": {
            "<VERSION>": "<URL FOR .1f DOWNLOAD>"
        }
    }
]
```

### Create Pack

> Create a new pack on Hearth

`POST /modpack/create`

Minimum Role: Guest

REQUEST:

```http
Content-Type: multipart/form-data
Content-Disposition: form-data; name="pack"
Content-Type: application/octet-stream

<CONTENTS OF .1f>
```

RESPONSE:

```json
{
    "id": <ID OF NEW PACK>,
    "title": "<TITLE OF NEW PACK>",
    "url": "<URL FOR PACK INFO>",
    "downloadUrl": "<URL FOR .1f DOWNLOAD>",
    "pending": true
}
```

### Add Users

> Add users to a pack

`PUT /modpack/<PACK ID>/users`

Minumum Role: Guest

REQUEST:

```json
[
    "<USERNAME OR UUID>"
]
```

## Moderation

### View Modqueue

> View current contents of the moderation queue

`GET /modqueue`

Minumum Role: Moderator

RESPONSE:

```json
[
    {
        "type": <EVENT TYPE>,
        "itemId": "<ITEM REFERENCED BY EVENT>",
        "notes": "<NOTES FOR EVENT>",
        "closed": false
    }
]
```

## Admin

### View Invites

> View the currently active invite keys

`GET /admin/invites`

Minimum Role: Admin

RESPONSE:

```json
[
    "<INVITE KEY>"
]
```

### Generate Invites

> Generate invite keys

`PUT /admin/invites`

Minimum Role: Admin

REQUEST (OPTIONAL):

```json
{
    "keyAmount": <NUMBER OF KEYS>
}
```

RESPONSE:

```
[
    "<INVITE KEY>"
]
```
