# Hearth Server Configuration File

### initialAdmin

`Type: string`

> Minecraft UUID of initial admin user

> Leave blank to disable

### webhookPath

`Type: string`

> Discord webhook path for moderation events

> Leave blank to disable

### databaseFile

`Type: string`

> Filename for Hearth database

### corsOrigin

`Type: list[string]`

> Allowed origins for CORS

> Leave empty to disable CORS

### maxFileSize

`Type: list[int]`

> Maximum single file size per role (in MiB)

### maxTotalSize

`Type: list[int]`

> Maximum total file size per role (in MiB)

### inviteRequired

`Type: bool`

> If an invitation is required to register

> The UUID defined in initialAdmin is exempt

### roleNames

`Type: list[string]`

> List of titles for roles

### fireGuardCommand

`Type: string`

> Command to run for upload scanning

> Leave blank to disable

> {FILE} is replaced with the path to file
