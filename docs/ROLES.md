# Hearth Roles

## Guest

* All uploaded files must be manually approved by a moderator
* Maximum single file size: 250mb
* Maximum total file size: 500mb

## User

* All new mods/modpacks must be manually approved by a moderator
* Maximum single file size: 250mb
* Maximum total file size: 5gb

## Trusted:

* Maximum single file size: 250mb
* Maximum total file size: 50gb

## Moderator:

* Can manage modqueue and users

## Admin:

* Literally god
