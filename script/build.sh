#!/usr/bin/env bash

# Dotnet info

dotnet --version

echo "Restoring packages..."

dotnet restore

echo "Building project..."

dotnet build -c Release

echo "Running tests..."

# TODO: Tests

echo "Publishing project..."

dotnet publish -c Release
